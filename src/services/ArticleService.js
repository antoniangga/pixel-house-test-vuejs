import axios from "axios";

const configApi = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

// POST - ARTICLE
const getPosts = (page, limit = 3) => {
  return configApi.get(`/posts?_start=${page}&_limit=${limit}`);
};

const getPost = (id) => {
  return configApi.get(`/posts/${id}`);
};

const createPost = (data) => {
  return configApi.post("/posts", {
    title: data.title,
    body: data.body,
    userId: data.userId,
  });
};

const updatePost = (data) => {
  return configApi.put(`/posts/${data.postId}`, {
    title: data.title,
    body: data.body,
    userId: data.userId,
  });
};

const deletePost = (id) => {
  return configApi.delete(`/posts/${id}`);
};

// USER
const getUsers = () => {
  return configApi.get("/users");
};

export default {
  getPosts,
  getPost,
  createPost,
  updatePost,
  deletePost,
  getUsers,
};
