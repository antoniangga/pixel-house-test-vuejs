import Vue from "vue";
import Vuex from "vuex";
import * as post from "@/store/modules/post.js";
import * as user from "@/store/modules/user.js";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    post,
  },
  state: {
    loading: false,
  },
  mutations: {
    SET_LOADING(state, event) {
      state.loading = event;
    },
  },
  actions: {
    changeLoading({ commit }, isLoading) {
      commit("SET_LOADING", isLoading);
    },
  },
});
