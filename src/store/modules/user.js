import ArticleService from "@/services/ArticleService.js";

export const namespaced = true;

export const state = {
  users: [],
};

export const mutations = {
  SET_USERS(state, users) {
    state.users = users;
  },
};
export const actions = {
  fetchUsers({ commit }) {
    return ArticleService.getUsers().then((response) => {
      commit("SET_USERS", response.data);
    });
  },
};
