import ArticleService from "@/services/ArticleService.js";

export const namespaced = true;

export const state = {
  posts: [],
  pagination: {
    currentPage: 1,
    limitPerPage: 3,
    totalPage: 0,
  },
  modal: {
    info: false,
  },
  post: "",
};

export const mutations = {
  SET_POSTS(state, posts) {
    state.posts = posts;
  },
  SET_PAGE_TOTAL(state, total) {
    state.pagination.totalPage = total;
  },
  SET_CURRENT_PAGE(state, current_page = 1) {
    state.pagination.currentPage = current_page;
  },
  SET_POST(state, post) {
    state.post = post;
  },
  SET_MODAL_INFO(state, toggle) {
    state.modal.info = toggle;
  },
};
export const actions = {
  fetchPosts({ commit, dispatch, state }, post) {
    ArticleService.getPosts(post.page, state.limitPerPage)
      .then((response) => {
        commit("SET_POSTS", response.data);
        commit("SET_PAGE_TOTAL", parseInt(response.headers["x-total-count"]));
        commit("SET_CURRENT_PAGE", parseInt(post.page));
        dispatch("changeLoading", false, { root: true });
      })
      .catch((err) => {
        // Notification here
        console.log(err);
      });
  },

  createPost({ commit }, dataPost) {
    return ArticleService.createPost(dataPost)
      .then((result) => {
        commit("SET_POST", result.data);
      })
      .catch((err) => {
        console.log(err);
      });
  },

  fetchPost({ commit, getters }, postId) {
    const post = getters.findById(postId);
    if (post) {
      commit("SET_POST", post);
      return post;
    } else {
      return ArticleService.getPost(postId).then((result) => {
        commit("SET_POST", result.data);
        return result.data;
      });
    }
  },

  toggleModal({ commit }, call) {
    commit("SET_MODAL_INFO", call);
  },

  removePost({ commit }, postId) {
    ArticleService.deletePost(postId).then(() => {
      commit("SET_MODAL_INFO", false);
    });
  },
};

export const getters = {
  findById: (state) => (id) => {
    return state.posts.find((post) => post.id === id);
  },
};
