import Vue from "vue";
import VueRouter from "vue-router";
import Post from "../views/Post.vue";
import CreatePost from "../views/CreatePost.vue";
import EditPost from "../views/EditPost.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/post/",
    name: "post",
    component: Post,
  },
  {
    path: "/post/create",
    name: "create-post",
    component: CreatePost,
  },
  {
    path: "/post/edit/:id",
    name: "update-post",
    component: EditPost,
  },
  { path: "/", redirect: { name: "post" } },
];

const router = new VueRouter({
  mode: "history",
  linkExactActiveClass: "is-active",
  routes,
});

export default router;
